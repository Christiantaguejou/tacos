module.exports = {
  assert: (what, something) => {
    if(something) {
        console.log("😀", what, "test is ✅")
    } else {
        console.log("😡", what, "test is 💥")
        process.exit(1)
    }
  }
}